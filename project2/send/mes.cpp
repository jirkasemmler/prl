/**
 * PRL Project 1
 * minimum extraction sort
 * Bc. Jiri Semmler , PRL 2016, FIT VUT (yes, I still waste my time with this crap)
 */
#include <mpi.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
using namespace std;

#define TAG 0
#define STOP -2
#define Empty -1

bool isRoot(int ID)
{
	return (ID == 0);
}

bool isLeaf(int ID, int n)
{
	return (ID >= (n - 1));
}

int getParentID(int myID)
{
	int tmpID = (myID == 0) ? 2 : myID;
	return (myID % 2 == 0) ? (tmpID - 2) / 2 : (tmpID - 1) / 2;
}

int getLeftLeaf(int myID)
{
	return (myID * 2 + 1);
}

int getRightLeaf(int myID)
{
	return (myID * 2 + 2);
}

/**
 * loads file "numbers" and sends values to all the leafs
 */
void loadFile(int n, int processorsCount, int originalN)
{
	char input[] = "numbers"; // file to read
	int tmpNumber;  // tmp var for receiving value from file
	int toSendID = n - 1;   // ID of processor where to send data to
	// ^^ write to leaf nodes only

	fstream fin;    // handler for file
	fin.open(input, ios::in); // open file
	vector<int> elements; // vector of elements got from file
	int i = 1;
	while (fin.good() && i <= originalN) // until it is able to read
	{
		i++;
		if (!fin.good())
		{
			break;
		}
		tmpNumber = fin.get(); // get one number

		MPI_Send(&tmpNumber, 1, MPI_INT, toSendID, TAG, MPI_COMM_WORLD);
		toSendID++;
		elements.push_back(tmpNumber); // push this elements to vector of elements
	}

	//<editor-fold desc="printing elements">
	for(size_t i = 0; i < elements.size(); ++i)
	{
		cout << elements[i]; // printing element
		if (i != tmpNumber)
		{
			cout << " "; // glue - the space between two elements
		}
	}
	//</editor-fold>

	fin.close();

	//<editor-fold desc="neutral values for empty leafs">
	// for case that I have less values that processors
	// (I should have
	while(toSendID <= (processorsCount-1))
	{
		tmpNumber = Empty;
		MPI_Send(&tmpNumber, 1, MPI_INT, toSendID, TAG, MPI_COMM_WORLD);
		toSendID++;
	}
	//</editor-fold>

	cout << "\n";
}

int main(int argc, char *argv[])
{
	double t1, t2;

	if(argc != 3)
	{
		return 1;
	}

	int processorsCount;  // tmpNumber of used processors
	int myID; // my rank
	int myValue = Empty;
	int leftChild, rightChild; // children's values
	int n = (atoi(argv[2])+1)/2;
	int originalN = atoi(argv[1]);
	int processorsOriginal = atoi(argv[2]);
	bool childrenEmpty = false;

	// check the number of cpu and elements to sord
	if((((processorsOriginal+1) & ((processorsOriginal+1) - 1)) != 0) || (2*originalN) > (processorsOriginal+1))
	{
		return 2;
	}

	//<editor-fold desc="initMPI">
	MPI_Status stat;
	MPI_Init(&argc, &argv);                        // Init MPI
	MPI_Comm_size(MPI_COMM_WORLD, &processorsCount);    // how many of processors are running
	MPI_Comm_rank(MPI_COMM_WORLD, &myID);        // what is myID of processor
	//</editor-fold>

	//<editor-fold desc="readFile">
	if (isRoot(myID)) // the 0 cpu is the main (and root)
	{
		loadFile(n, processorsCount, originalN);
		t1 = MPI_Wtime();

	}
	//</editor-fold>

	if (isLeaf(myID, n)) // leaf nodes
	{
		MPI_Recv(&myValue, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat); // as leafs - receiving values from file as
	}

	bool stopMe = false;
	
	while (!stopMe)
	{
		if (!isLeaf(myID,n))
		{
			if (isRoot(myID)) // I have something in the root -> print it out
			{
				if (myValue >= 0)
				{
					cout << myValue << "\n"; // print
					myValue = Empty; // set empty again
				}
			}

			if (myValue == Empty && !childrenEmpty) // I, as non-leaf, am empty and my children are not empty
			{
				// receive data from children
				MPI_Recv(&leftChild, 1, MPI_INT, getLeftLeaf(myID), TAG, MPI_COMM_WORLD, &stat);
				MPI_Recv(&rightChild, 1, MPI_INT, getRightLeaf(myID), TAG, MPI_COMM_WORLD, &stat);

				if (leftChild == Empty && rightChild != Empty) // left child is empty
				{
					myValue = rightChild; // get value from right child
					rightChild = Empty; // clean right child
				}
				else if (leftChild != Empty && rightChild == Empty) // analogy from right ^^
				{
					myValue = leftChild;
					leftChild = Empty;
				}
				else if (leftChild == Empty && rightChild == Empty) // both of them are empty -> set this node as empty
				{
					leftChild = STOP; // stop children. see @stoppingProcessor
					rightChild = STOP;
					childrenEmpty = true; // my children are empty
					if (isRoot(myID))
					{
						stopMe = true; // stop this processor
					}
				}

				else if (leftChild < rightChild) // they are not empty -> compare values
				{
					myValue = leftChild;
					leftChild = Empty;
				}
				else
				{
					myValue = rightChild;
					rightChild = Empty;
				}

				// inform children about their future value
				MPI_Send(&leftChild, 1, MPI_INT, getLeftLeaf(myID), TAG, MPI_COMM_WORLD);
				MPI_Send(&rightChild, 1, MPI_INT, getRightLeaf(myID), TAG, MPI_COMM_WORLD);
			}
		}

		if (!isRoot(myID))
		{
			// me as random node (not the root) send my value to parent (root has no parent)
			MPI_Send(&myValue, 1, MPI_INT, getParentID(myID), TAG, MPI_COMM_WORLD);
			// receive (and update) new value from parent
			MPI_Recv(&myValue, 1, MPI_INT, getParentID(myID), TAG, MPI_COMM_WORLD, &stat);

			if (myValue == STOP) // If parent told me I should stop -> leave the loop and stop @stoppingProcessor
			{
				stopMe = true;
			}
		}
	}

	if(isRoot(myID))
	{
		t2 = MPI_Wtime();
		// if you want to measure the time, uncomment following line
//		cout << "time" << (t2-t1) << "\n";
	}
	MPI_Finalize();
	return 0;
}