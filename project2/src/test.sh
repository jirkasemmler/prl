#!/bin/bash

if [ $# -eq 2 ];then
    numbers=$1
    procesors=$2
else
    exit
fi;
# mes = Minimum Extraction Sort

#build
mpic++ --prefix /usr/local/share/OpenMPI -o mes main.cpp > /dev/null 2>&1

#prepare the sequence of number to sort
dd if=/dev/random bs=1 count=$numbers of=numbers > /dev/null 2>&1

# run it!
mpirun --prefix /usr/local/share/OpenMPI -np $procesors mes $numbers $procesors

# clean
rm -f mes numbers

