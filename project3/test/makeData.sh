#!/bin/bash
mkdir num4
mkdir num8
mkdir num16
mkdir num32
mkdir num64

for i in {1..1000};
do
  echo "obase=2;$((RANDOM%16))" | bc >> ./num4/numbers$i;
  echo "obase=2;$((RANDOM%16))" | bc >> ./num4/numbers$i;
done

for i in {1..1000};
do
  echo "obase=2;$((RANDOM%256))" | bc >> ./num8/numbers$i;
  echo "obase=2;$((RANDOM%256))" | bc >> ./num8/numbers$i;
done

for i in {1..1000};
do
  echo "obase=2;$((RANDOM%65536))" | bc >> ./num16/numbers$i;
  echo "obase=2;$((RANDOM%65536))" | bc >> ./num16/numbers$i;
done

for i in {1..1000};
do
  echo "obase=2;$((RANDOM%4294967296))" | bc >> ./num32/numbers$i;
  echo "obase=2;$((RANDOM%4294967296))" | bc >> ./num32/numbers$i;
done


for i in {1..1000};
do
  echo "obase=2;$((RANDOM%4294967296))" | bc >> ./num64/numbers$i;
  echo "obase=2;$((RANDOM%4294967296))" | bc >> ./num64/numbers$i;
done
