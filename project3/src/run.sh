#!/bin/bash

echo "" >  output4.txt
for i in {1..500};
do
 cat ./../test/num4/numbers$i >> numbers
 sh test.sh 7 >> output4.txt
 rm numbers
done

echo "" >  output8.txt
for i in {1..500};
do
 cat ./../test/num8/numbers$i >> numbers
 sh test.sh 15 >> output8.txt
 rm numbers
done

echo "" >  output16.txt

for i in {1..500};
do
 cat ./../test/num16/numbers$i >> numbers
 sh test.sh 31 >> output16.txt
 rm numbers
done

echo "" >  output32.txt

for i in {1..500};
do
 cat ./../test/num32/numbers$i >> numbers
 sh test.sh 63 >> output32.txt
 rm numbers
done


echo "" >  output64.txt

for i in {1..500};
do
  cat ./../test/num64/numbers$i >> numbers
  sh test.sh 127 >> output64.txt
  rm numbers
done