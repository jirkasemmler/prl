/**
 * PRL Project 3
 * carry something
 * Bc. Jiri Semmler , PRL 2016, FIT VUT (yes, I still waste my time with this crap)
 */
#include <mpi.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
using namespace std;
#define toDigit(c) (c-'0')
#define TAG 0
#define S 0
#define P 1
#define G 2


int mat[3][3] = { S, S, S, S, P, G, G, G, G};

//double t1, t2;

bool isRoot(int ID)
{
	return (ID == 0);
}

bool isLeaf(int ID, int n)
{
	return (ID >= (n - 1));
}

int getParentID(int myID)
{
	int tmpID = (myID == 0) ? 2 : myID;
	return (myID % 2 == 0) ? (tmpID - 2) / 2 : (tmpID - 1) / 2;
}

int getLeftLeaf(int myID)
{
	return (myID * 2 + 1);
}

int getRightLeaf(int myID)
{
	return (myID * 2 + 2);
}

int getLeftNeighbour(int myID)
{
	return (myID - 1);
}
int getRightNeighbour(int myID)
{
	return (myID + 1);
}

int operation(int rowIndex, int colIndex)
{
	return mat[rowIndex][colIndex];
}

char translate(int i)
{
	string a = "spg";
	return a[i];
}

int itemToInt(int x)
{
	return (x == G) ? 1 : 0;
}

void distribute(string in, int processorsCount)
{
	int lengthOfString = (processorsCount+1) / 2;
	if(in.length() < lengthOfString)
	{
		int toAdd = lengthOfString - in.length();
		string toAddStr(toAdd, '0'); // prepend number with zeros
		in = toAddStr + in;
	}
//	t1 = MPI_Wtime(); // for testing
	for(int i = 0; i<lengthOfString ; i++)
	{
		int index = processorsCount - in.length() + i;
		int tmp = toDigit(in[i]);
		MPI_Send( &tmp, 1, MPI_INT, index , TAG, MPI_COMM_WORLD);
	}
}

bool readFileAndDistribute(string filePath, int processorsCount)
{
	string line1, line2;
	ifstream myfile (filePath.c_str()); // read the file
	if (myfile.is_open())
	{
		getline (myfile,line1); // read the first two lines only
		getline (myfile,line2);

		int bigger = max(line1.length(), line2.length());
		if (bigger > (processorsCount+1)/2)
		{
			cout << "one of the numbers if tooooo long";
			return false;
		}

		distribute(line1, processorsCount);
		distribute(line2, processorsCount);
	}
	else
	{
		cout << "Unable to open file";
		return false;
	}
	return true;
}

int main(int argc, char *argv[])
{

	int myX, myY;
	if(argc != 2)
	{
		cout << "wrong input";
		return 1;
	}
	int processorsCount;  // tmpNumber of used processors
	int myID; // my rank

	int myVal;
	int left, right;

	MPI_Status stat;
	MPI_Init(&argc, &argv);                        // Init MPI
	MPI_Comm_size(MPI_COMM_WORLD, &processorsCount);    // how many of processors are running
	MPI_Comm_rank(MPI_COMM_WORLD, &myID);        // what is myID of processor
	if(myID == 0) // root
	{
		readFileAndDistribute(argv[1], processorsCount); // distribute data to leafs


		//<editor-fold desc="up-sweep-root">
		MPI_Recv(&left, 1, MPI_INT, getLeftLeaf(myID), TAG, MPI_COMM_WORLD, &stat); // wait for left child
		MPI_Recv(&right, 1, MPI_INT, getRightLeaf(myID), TAG, MPI_COMM_WORLD, &stat); // wait for right child
		//</editor-fold>

		//<editor-fold desc="root-change-over">
		myVal = operation(left, right);
		int tmp = myVal; // store the value I counted from childs
		myVal = P; // set the neutral value

		left = operation(right , myVal);
		right = myVal;
		//</editor-fold>

		//<editor-fold desc="down-sweep-root">
		MPI_Send( &left, 1, MPI_INT, getLeftLeaf(myID) , TAG, MPI_COMM_WORLD);
		MPI_Send( &right, 1, MPI_INT, getRightLeaf(myID) , TAG, MPI_COMM_WORLD);
		//</editor-fold>

		// shift
		MPI_Send( &tmp, 1, MPI_INT, ((processorsCount-1)/2) , TAG, MPI_COMM_WORLD); // fill the left child (after shift)

		//<editor-fold desc="timeTesting">
//		double tmpTime = 0;
//		double max = 0;
//		for(int i = (processorsCount - 1)/2; i < (processorsCount - 1) ; i++)
//		{
//			MPI_Recv(&tmpTime, 1, MPI_DOUBLE,i, TAG, MPI_COMM_WORLD, &stat);
//			if(tmpTime > max)
//			{
//				max = tmpTime;
//			}
//		}
//		cout << (max - t1) << " \n";
		//</editor-fold>
	}
	else if (myID >= ((processorsCount - 1 ) / 2)) // leaf
	{
		MPI_Recv(&myX, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat); // as leafs - receiving value
		MPI_Recv(&myY, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat); // as leafs - receiving value

		if (myX == myY) // from slides
		{
			myVal = (myX == 1) ? G : S;
		}
		else
		{
			myVal = P;
		}



		//<editor-fold desc="prescan-leaf">
		MPI_Send( &myVal, 1, MPI_INT, getParentID(myID) , TAG, MPI_COMM_WORLD); // up sweep
		MPI_Recv(&myVal, 1, MPI_INT, getParentID(myID), TAG, MPI_COMM_WORLD, &stat); // down sweep
		//</editor-fold>



		//<editor-fold desc="scan-leaf">
		if(myID != (processorsCount-1)) // I am NOT the right leaf
		{
			MPI_Send( &myVal, 1, MPI_INT, getRightNeighbour(myID) , TAG, MPI_COMM_WORLD);
		}

		if(myID != ((processorsCount-1)/2)) // I am NOT the left leaf
		{
			MPI_Recv(&myVal, 1, MPI_INT, getLeftNeighbour(myID), TAG, MPI_COMM_WORLD, &stat);
		}
		else // I am the left leaf
		{
			MPI_Recv(&myVal, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat);
		}
		//</editor-fold>



		// posledni shift doleva

		if(myID != ((processorsCount-1)/2)) // I am NOT the left leaf
		{
			MPI_Send( &myVal, 1, MPI_INT, getLeftNeighbour(myID) , TAG, MPI_COMM_WORLD);
		}

		if(myID != (processorsCount-1)) // I am NOT the right leaf
		{
			if(myID == ((processorsCount-1)/2)) // I am the left leaf -> overflow detection
			{
				if(myVal == G)
				{
					cout << "overflow\n";
				}
			}
			MPI_Recv(&myVal, 1, MPI_INT, getRightNeighbour(myID), TAG, MPI_COMM_WORLD, &stat);
		}
		else if(myID == (processorsCount-1))// I am on the right end, fill by zero
		{
			myVal = 0;
		}

		int bin = (myX + myY + itemToInt(myVal))%2; // count the z_i
		cout << myID << ":" << bin << "\n";

		//<editor-fold desc="timeTesting">
//		t1 = MPI_Wtime();
//		MPI_Send( &t1, 1, MPI_DOUBLE, 0 , TAG, MPI_COMM_WORLD);
		//</editor-fold>


	}
	else // non leaf
	{
		MPI_Recv(&left, 1, MPI_INT, getLeftLeaf(myID), TAG, MPI_COMM_WORLD, &stat); // receive from the left leaf
		MPI_Recv(&right, 1, MPI_INT, getRightLeaf(myID), TAG, MPI_COMM_WORLD, &stat); // receive from the right leaf

		myVal = operation( left, right);

		MPI_Send( &myVal, 1, MPI_INT, getParentID(myID) , TAG, MPI_COMM_WORLD);

		//<editor-fold desc="prescanScanNOTleaf">
		MPI_Recv(&myVal, 1, MPI_INT, getParentID(myID), TAG, MPI_COMM_WORLD, &stat);

		left = operation(right, myVal);
		right = myVal;

		MPI_Send( &left, 1, MPI_INT, getLeftLeaf(myID) , TAG, MPI_COMM_WORLD);
		MPI_Send( &right, 1, MPI_INT, getRightLeaf(myID) , TAG, MPI_COMM_WORLD);
		//</editor-fold>
	}

	// finish him!
	MPI_Finalize();
	return 0;


}